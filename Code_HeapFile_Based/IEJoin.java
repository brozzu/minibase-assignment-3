package iterator;

import heap.*;
import global.*;
import bufmgr.*;
import diskmgr.*;
import index.*;
import java.lang.*;
import java.net.SocketImpl;
import java.nio.file.attribute.FileStoreAttributeView;
import java.io.*;

import java.util.*;

/**
 *
 * This file contains an implementation of the nested loops join algorithm as
 * described in the Shapiro paper. The algorithm is extremely simple:
 *
 * foreach tuple r in R do foreach tuple s in S do if (ri == sj) then add (r, s)
 * to the result.
 */

public class IEJoin extends Iterator {
    private AttrType _in1[], _in2[];
    private short[] _t1StrSize, _t2StrSize;
    private Tuple Jtuple; // Joined tuple
    private FldSpec perm_mat[];
    private int nOutFlds;
    private ArrayList<RID> L1, L1p, L2, L2p;
    private ArrayList<Tuple> L1_t,L1p_t,L2_t,L2p_t;
    private int[] P, Pp, O, O2;
    private short[] Bp;
    private BitMap bitmap;
    private java.util.Iterator<Integer> bit_out;
    private int n, m, i;
    private short eqOff;
    private boolean getNextUsed, i_visited;

    private Heapfile sourceFile1, sourceFile2;
    private Tuple outerTuple;

    /**
     * constructor Initialize the two relations which are joined, including relation
     * type,
     * 
     * @param in1          Array containing field types of R.
     * @param len_in1      # of columns in R.
     * @param t1_str_sizes shows the length of the string fields.
     * @param in2          Array containing field types of S
     * @param len_in2      # of columns in S
     * @param t2_str_sizes shows the length of the string fields.
     * @param amt_of_mem   IN PAGES
     * @param am1          access method for left i/p to join
     * @param relationName access hfapfile for right i/p to join
     * @param outFilter    select expressions
     * @param rightFilter  reference to filter applied on right i/p
     * @param proj_list    shows what input fields go where in the output tuple
     * @param n_out_flds   number of outer relation fileds
     * @throws Exception
     * @throws UnknownKeyTypeException
     * @throws UnknowAttrType
     * @throws LowMemException
     * @throws PredEvalException
     * @throws TupleUtilsException
     * @throws PageNotReadException
     * @throws InvalidTypeException
     * @throws InvalidTupleSizeException
     * @throws IndexException
     * @throws JoinsException
     * @exception IEJoinException exception from this class (To Implement)
     */
    public IEJoin(AttrType in1[], int len_in1, short t1_str_sizes[], AttrType in2[], int len_in2, short t2_str_sizes[],
            int amt_of_mem, String heapfile1, String heapfile2, CondExpr outFilter[], CondExpr rightFilter[],
            FldSpec proj_list[], int n_out_flds) throws JoinsException, IndexException, InvalidTupleSizeException,
            InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, LowMemException,
            UnknowAttrType, UnknownKeyTypeException, Exception {

        // Flag
        getNextUsed = false;

        // Set Offset Based on Predicates
        eqOff = SetOffset(outFilter);

        // Set Ascending based on Op
        TupleOrder[] sortingDir = SetSortingDirection(outFilter);
        TupleOrder[] secondaryKeyDir = SetSecondarySortingDirection(outFilter);
        
        //Create Tmp file for source1
        Tuple tuple = null;
        RID rid = new RID();

        sourceFile1 = new Heapfile(heapfile1);
        sourceFile2 = new Heapfile(heapfile2);

        AttrType[] tmpTypes = new AttrType[4];
            tmpTypes[0] = new AttrType(AttrType.attrInteger);
            tmpTypes[1] = new AttrType(AttrType.attrInteger);
            tmpTypes[2] = new AttrType(AttrType.attrInteger);
            tmpTypes[3] = new AttrType(AttrType.attrInteger);

        short[] tmpStrSizes =  new short[1];
            tmpStrSizes[0] = 0;

        FldSpec[] Sprojection = new FldSpec[tmpTypes.length];
            for (int i = 0; i < tmpTypes.length; i++) {
                Sprojection[i] = new FldSpec(new RelSpec(RelSpec.outer), i + 1);
            }

        Heapfile tmpfile1 = new Heapfile("file1.tmp");
        Scan sourceScan = sourceFile1.openScan();
        while((tuple = sourceScan.getNext(rid))!=null){
            tuple.setHdr((short) in1.length, in1, t1_str_sizes);

            Tuple tmp = new Tuple();
            tmp.setHdr((short) tmpTypes.length, tmpTypes, tmpStrSizes);

            tmp.setIntFld(1, rid.pageNo.pid);
            tmp.setIntFld(2, rid.slotNo);
            tmp.setIntFld(3, tuple.getIntFld(outFilter[0].operand1.symbol.offset));
            tmp.setIntFld(4, tuple.getIntFld(outFilter[1].operand1.symbol.offset));

            tmpfile1.insertRecord(tmp.getTupleByteArray());
        }


        Heapfile tmpfile2 = new Heapfile("file2.tmp");
        sourceScan = sourceFile2.openScan();
        while((tuple = sourceScan.getNext(rid))!=null){
            tuple.setHdr((short) in2.length, in2, t2_str_sizes);

            Tuple tmp = new Tuple();
            tmp.setHdr((short) tmpTypes.length, tmpTypes, tmpStrSizes);

            tmp.setIntFld(1, rid.pageNo.pid);
            tmp.setIntFld(2, rid.slotNo);
            tmp.setIntFld(3, tuple.getIntFld(outFilter[0].operand2.symbol.offset));
            tmp.setIntFld(4, tuple.getIntFld(outFilter[1].operand2.symbol.offset));

            tmpfile2.insertRecord(tmp.getTupleByteArray());
        }


        // Create the 4 iterators to be sorted
        FileScan[] file1Itr = new FileScan[2];
        file1Itr[0] = new FileScan("file1.tmp", tmpTypes, tmpStrSizes, (short) tmpTypes.length, 
                                    (short) tmpTypes.length, Sprojection, null);
        file1Itr[1] = new FileScan("file1.tmp", tmpTypes, tmpStrSizes, (short) tmpTypes.length, 
                                    (short) tmpTypes.length, Sprojection, null);

        FileScan[] file2Itr = new FileScan[2];
        file2Itr[0] = new FileScan("file2.tmp", tmpTypes, tmpStrSizes, (short) tmpTypes.length, 
                                    (short) tmpTypes.length, Sprojection, null);
        file2Itr[1] = new FileScan("file2.tmp", tmpTypes, tmpStrSizes, (short) tmpTypes.length, 
                                    (short) tmpTypes.length, Sprojection, null);



        //Sorting Phase: Double merge sort to avoid duplicate problems
 
        Iterator tmp = null;
        AttrType[] tmpTupleTypes = new AttrType[2];
            tmpTupleTypes[0] = new AttrType(AttrType.attrInteger);
            tmpTupleTypes[1] = new AttrType(AttrType.attrInteger);

        short[] tmpTupleStrSizes =  new short[1];
            tmpStrSizes[0] = 0;

        tmp = new Sort( tmpTypes, (short) tmpTypes.length, tmpStrSizes, file1Itr[0], 4,
                        secondaryKeyDir[0], tmpStrSizes[0], amt_of_mem);
        Iterator L1Itr = new Sort( tmpTypes, (short) tmpTypes.length, tmpStrSizes, tmp, 3,
                        sortingDir[0], tmpStrSizes[0], amt_of_mem);

        L1 = new ArrayList<RID>();
        L1_t = new ArrayList<Tuple>();
        tuple = null;
        while ((tuple = L1Itr.get_next()) != null) {
            //tuple.print(types);
            PageId pageId = new PageId( tuple.getIntFld(1));
            int slotNo = tuple.getIntFld(2);
            RID tmp_rid = new RID(pageId,slotNo);
            L1.add(tmp_rid);

            Tuple tmpTuple = new Tuple();
            tmpTuple.setHdr((short)tmpTupleTypes.length, tmpTupleTypes, tmpTupleStrSizes);

            tmpTuple.setIntFld(1, tuple.getIntFld(3));
            tmpTuple.setIntFld(2, tuple.getIntFld(4));
            L1_t.add(tmpTuple);
        }
 
        //L1Itr.close();
        //file1Itr[0].close();

        tmp = new Sort( tmpTypes, (short) tmpTypes.length, tmpStrSizes, file1Itr[1], 3,
                        secondaryKeyDir[1], tmpStrSizes[0], amt_of_mem);
        Iterator L2Itr = new Sort( tmpTypes, (short) tmpTypes.length, tmpStrSizes, tmp, 4,
                        sortingDir[1], tmpStrSizes[0], amt_of_mem);

        L2 = new ArrayList<RID>();
        L2_t = new ArrayList<Tuple>();
        tuple = null;
        while ((tuple = L2Itr.get_next()) != null) {
            //tuple.print(types);
            PageId pageId = new PageId( tuple.getIntFld(1));
            int slotNo = tuple.getIntFld(2);
            RID tmp_rid = new RID(pageId,slotNo);
            L2.add(tmp_rid);

            Tuple tmpTuple = new Tuple();
            tmpTuple.setHdr((short)tmpTupleTypes.length, tmpTupleTypes, tmpTupleStrSizes);

            tmpTuple.setIntFld(1, tuple.getIntFld(3));
            tmpTuple.setIntFld(2, tuple.getIntFld(4));
            L2_t.add(tmpTuple);
        }

        //L2Itr.close();
        //file1Itr[1].close();

        tmp = new Sort( tmpTypes, (short) tmpTypes.length, tmpStrSizes, file2Itr[0], 4,
                        secondaryKeyDir[0], tmpStrSizes[0], amt_of_mem);
        Iterator L1pItr = new Sort( tmpTypes, (short) tmpTypes.length, tmpStrSizes, tmp, 3,
                        sortingDir[0], tmpStrSizes[0], amt_of_mem);

        L1p = new ArrayList<RID>();
        L1p_t = new ArrayList<Tuple>();
        tuple = null;
        while ((tuple = L1pItr.get_next()) != null) {
            //tuple.print(types);
            PageId pageId = new PageId( tuple.getIntFld(1));
            int slotNo = tuple.getIntFld(2);
            RID tmp_rid = new RID(pageId,slotNo);
            L1p.add(tmp_rid);

            Tuple tmpTuple = new Tuple();
            tmpTuple.setHdr((short)tmpTupleTypes.length, tmpTupleTypes, tmpTupleStrSizes);

            tmpTuple.setIntFld(1, tuple.getIntFld(3));
            tmpTuple.setIntFld(2, tuple.getIntFld(4));
            L1p_t.add(tmpTuple);
        }


        //L1pItr.close();
        //file2Itr[0].close();

        tmp = new Sort( tmpTypes, (short) tmpTypes.length, tmpStrSizes, file2Itr[1], 3,
                        secondaryKeyDir[1], tmpStrSizes[0], amt_of_mem);
        Iterator L2pItr = new Sort( tmpTypes, (short) tmpTypes.length, tmpStrSizes, tmp, 4,
                        sortingDir[1], tmpStrSizes[0], amt_of_mem);

        L2p = new ArrayList<RID>();
        L2p_t = new ArrayList<Tuple>();
        tuple = null;
        while ((tuple = L2pItr.get_next()) != null) {
            //tuple.print(types);
            PageId pageId = new PageId( tuple.getIntFld(1));
            int slotNo = tuple.getIntFld(2);
            RID tmp_rid = new RID(pageId,slotNo);
            L2p.add(tmp_rid);

            Tuple tmpTuple = new Tuple();
            tmpTuple.setHdr((short)tmpTupleTypes.length, tmpTupleTypes, tmpTupleStrSizes);

            tmpTuple.setIntFld(1, tuple.getIntFld(3));
            tmpTuple.setIntFld(2, tuple.getIntFld(4));
            L2p_t.add(tmpTuple);
        }


        //L2pItr.close();
        //file2Itr[1].close();

        m = L1.size();
        n = L1p.size();

        P = createPermutationArray(L2, L1, m, in1);
        Pp = createPermutationArray(L2p, L1p, n, in2);

        O = createOffsetArray(tmpTupleTypes, L1_t, tmpTupleTypes, L1p_t, 1, 1, sortingDir[0]);
        O2 = createOffsetArray(tmpTupleTypes, L2_t, tmpTupleTypes, L2p_t, 2, 2, sortingDir[1]);

        bitmap = new BitMap(n,10);

        // Copy field types
        _in1 = new AttrType[in1.length];
        _in2 = new AttrType[in2.length];
        System.arraycopy(in1, 0, _in1, 0, in1.length);
        System.arraycopy(in2, 0, _in2, 0, in2.length);
        _t1StrSize = new short[t1_str_sizes.length];
        _t2StrSize = new short[t2_str_sizes.length];
        System.arraycopy(t1_str_sizes, 0, _t1StrSize, 0, t1_str_sizes.length);
        System.arraycopy(t2_str_sizes, 0, _t2StrSize, 0, t2_str_sizes.length);
      
        Jtuple = new Tuple();
    
        AttrType[] Jtypes = new AttrType[n_out_flds];
        short[] t_size;

        perm_mat = proj_list;
        nOutFlds = n_out_flds;

        try {
            t_size = TupleUtils.setup_op_tuple(Jtuple, Jtypes, in1, len_in1, in2, len_in2, t1_str_sizes, t2_str_sizes,
                    proj_list, nOutFlds);
        } catch (TupleUtilsException e) {
            // throw new IEJoinException(e, "TupleUtilsException is caught by
            // IEJoin.java");
        }

        tmpfile1.deleteFile();
        tmpfile2.deleteFile();
    }

    /**
     * @exception PageNotReadException    exception from lower layer
     * @exception TupleUtilsException     exception from using tuple utilities
     * @exception PredEvalException       exception from PredEval class
     * @exception SortException           sort exception
     * @exception LowMemException         memory error
     * @exception UnknowAttrType          attribute type unknown
     * @exception UnknownKeyTypeException key type unknown
     * @exception Exception               other exceptions
     * 
     */
    public Tuple get_next() throws IOException, JoinsException, IndexException, InvalidTupleSizeException,
            InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, SortException,
            LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception {

        if (!getNextUsed) {
            i = 0;
            i_visited = false;
            getNextUsed = true;
        }

        while (i < m) {

            if (!i_visited) {
                // Set Bit relative to visited Tuples in L1p
                int off2 = O2[i]; // L2 -> L2p
                for (int k = 0; k < Math.min(off2, n); k++) {
                    bitmap.setBit(Pp[k]);
                }

                int off1 = O[P[i]]; // L1 -> L1p
                bit_out = bitmap.getValidPositions(off1 + eqOff);

                outerTuple = sourceFile1.getRecord(L2.get(i));
                outerTuple.setHdr((short)_in1.length, _in1, _t1StrSize);

                i_visited = true;
            }

            
            if(bit_out.hasNext()){
                int j = bit_out.next();

                Tuple innerTuple = sourceFile2.getRecord(L1p.get(j));
                innerTuple.setHdr((short)_in2.length, _in2, _t2StrSize);

                Projection.Join(innerTuple, _in1, outerTuple, _in2, Jtuple, perm_mat, nOutFlds);
                return Jtuple;

            }


            i++;
            i_visited = false;
        }

        return null;
    }

    /**
     * implement the abstract method close() from super class Iterator to finish
     * cleaning up
     * 
     * @exception IOException    I/O error from lower layers
     * @exception JoinsException join error from lower layers
     * @exception IndexException index access error
     */
    public void close() throws JoinsException, IOException, IndexException {
        if (!closeFlag) {

            try {
                //outer.close();
            } catch (Exception e) {
                throw new JoinsException(e, "IEJoin.java: error in closing iterator.");
            }
            closeFlag = true;
        }
    }


    private int[] createPermutationArray(ArrayList<RID> list_1, ArrayList<RID> list_2, int rel_len, AttrType[] in)
            throws UnknowAttrType, TupleUtilsException, IOException {
        int[] perm_arr = new int[rel_len];
        for (int i = 0; i < rel_len; i++) {
            for (int j = 0; j < rel_len; j++) {
                RID rid1 = list_1.get(i);
                RID rid2 = list_2.get(j);

                if( rid1.pageNo.pid == rid2.pageNo.pid && rid1.slotNo == rid2.slotNo){
                    perm_arr[i] = j;
                    break;
                }
            }
        }

        return perm_arr;
    }

    private int[] createOffsetArray(AttrType[] in1, ArrayList<Tuple> list_1, AttrType[] in2, ArrayList<Tuple> list_2,
            int offset_1, int offset_2, TupleOrder Sorting_order)
            throws UnknowAttrType, TupleUtilsException, IOException {
        int[] Offset = new int[list_1.size()];

        for (int i = 0; i < list_1.size(); i++) {
            boolean found = false;

            for (int j = 0; j < list_2.size() && !found; j++) {
                // Compare each field

                if (((Sorting_order.tupleOrder == TupleOrder.Descending)
                        && TupleUtils.CompareTupleWithTuple(in1[offset_1 - 1], list_1.get(i), offset_1, list_2.get(j),
                                offset_2) >= 0)
                        || ((Sorting_order.tupleOrder == TupleOrder.Ascending)
                                && TupleUtils.CompareTupleWithTuple(in1[offset_1 - 1], list_1.get(i), offset_1,
                                        list_2.get(j), offset_2) <= 0)) {
                    found = true;
                    Offset[i] = j;
                    break;
                }
            }

            if (!found) {
                Offset[i] = list_1.size();
            }

        }

        return Offset;
    }

    private short SetOffset(CondExpr outFilter[]) {
        short offset = 0;

        for (int i = 0; i < (outFilter.length - 1); i++) {
            String op = outFilter[i].op.toString();

            switch (op) {

                case "aopLT":
                    offset = 1;
                    break;

                case "aopGT":
                    offset = 1;
                    break;

                default:
                    break;
            }
        }

        return offset;
    }


    private TupleOrder[] SetSortingDirection(CondExpr outFilter[]) {
        TupleOrder[] sortingDir = new TupleOrder[outFilter.length - 1];

        for (int i = 0; i < (outFilter.length - 1); i++) {
            String op = outFilter[i].op.toString();

            switch (op) {

                case "aopLT":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Descending : TupleOrder.Ascending);
                    break;

                case "aopLE":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Descending : TupleOrder.Ascending);
                    break;

                case "aopGT":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Ascending : TupleOrder.Descending);
                    break;

                case "aopGE":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Ascending : TupleOrder.Descending);
                    break;

                default:
                    System.out.println("The FilterOut contains the " + op + " operator");
                    break;
            }
        }
        return sortingDir;
    }

    private TupleOrder[] SetSecondarySortingDirection(CondExpr outFilter[]) {
        TupleOrder[] sortingDir = new TupleOrder[2];


        switch(outFilter[0].op.toString()){
            case "aopLT":
                switch (outFilter[1].op.toString()){
                    case "aopLT":
                        sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                        sortingDir[1] = new TupleOrder(TupleOrder.Descending);
                        break;

                    case "aopLE":
                        sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                        sortingDir[1] = new TupleOrder(TupleOrder.Ascending);
                        break;

                    case "aopGT":
                        sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                        sortingDir[1] = new TupleOrder(TupleOrder.Descending);
                        break;

                    case "aopGE":
                        sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                        sortingDir[1] = new TupleOrder(TupleOrder.Ascending);
                        break;

                    default:
                        System.out.println("The FilterOut contains the not expected operator");
                        break;
                }
                break;

                case "aopLE":
                    switch (outFilter[1].op.toString()){
                        case "aopLT":
                            sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Descending);
                            break;

                        case "aopLE":
                            sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Ascending);
                            break;

                        case "aopGT":
                            sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Descending);
                            break;

                        case "aopGE":
                            sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Ascending);                            
                        break;

                        default:
                            System.out.println("The FilterOut contains the not expected operator");        
                            break;
                    }
                    break;

                case "aopGT":
                    switch (outFilter[1].op.toString()){
                        case "aopLT":
                            sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Ascending);                            
                            break;

                        case "aopLE":
                            sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Descending);                            
                            break;

                        case "aopGT":
                            sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Ascending);                            
                            break;

                        case "aopGE":
                            sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Descending);                            
                        break;

                        default:
                            System.out.println("The FilterOut contains the not expected operator");
                            break;
                    }
                    break;

                case "aopGE":
                    switch (outFilter[1].op.toString()){
                        case "aopLT":
                            sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Ascending);                            
                            break;

                        case "aopLE":
                            sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Descending);                            
                            break;

                        case "aopGT":
                            sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Ascending);                            
                            break;

                        case "aopGE":
                            sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Descending);                            
                            break;

                        default:
                            System.out.println("The FilterOut contains the not expected operator");
                            break;
                    }
                    break;

                default:
                    System.out.println("The FilterOut contains the not expected operator");
                    break;
            
        }


        for (int i = 0; i < (outFilter.length - 1); i++) {
            String op = outFilter[i].op.toString();

            switch (op) {

                case "aopLT":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Descending : TupleOrder.Ascending);
                    break;

                case "aopLE":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Descending : TupleOrder.Ascending);
                    break;

                case "aopGT":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Ascending : TupleOrder.Descending);
                    break;

                case "aopGE":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Ascending : TupleOrder.Descending);
                    break;

                default:
                    System.out.println("The FilterOut contains the " + op + " operator");
                    break;
            }
        }
        return sortingDir;
    }


    private void print_tuple_arrayList(ArrayList<Tuple> L, AttrType[] in) {
        L.stream().forEach(item -> {
            // Compare each field

            try {
                item.print(in);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        });
    }

}
