package iterator;

import heap.*;
import global.*;
import bufmgr.*;
import diskmgr.*;
import index.*;
import java.lang.*;
import java.io.*;

import java.util.*;

/**
 *
 * This file contains an implementation of the nested loops join algorithm as
 * described in the Shapiro paper. The algorithm is extremely simple:
 *
 * foreach tuple r in R do foreach tuple s in S do if (ri == sj) then add (r, s)
 * to the result.
 */

public class IESelfJoin2 extends Iterator {
    private AttrType _in1[], _in2[];
    private short[] _t1StrSizes;
    private Iterator outer;
    private Tuple Jtuple; // Joined tuple
    private FldSpec perm_mat[];
    private int nOutFlds;

    // Aggiunte da Brozzu
    private ArrayList<Tuple> list_1, list_2;
    private int[] perm_arr;
    private int rel_len;
    private BitMap bitmap;
    private boolean visited, first;
    private java.util.Iterator<Integer> bit_out;

    private int L1pos;
    private int L2pos;
    private short Off;
    private Tuple outerTuple;

    private ArrayList<RID> sorted1, sorted2;

    private Heapfile sourceFile;

    /**
     * constructor Initialize the two relations which are joined, including relation
     * type,
     * 
     * @param in1          Array containing field types of R.
     * @param len_in1      # of columns in R.
     * @param t1_str_sizes shows the length of the string fields.
     * @param in2          Array containing field types of S
     * @param len_in2      # of columns in S
     * @param t2_str_sizes shows the length of the string fields.
     * @param amt_of_mem   IN PAGES
     * @param am1          access method for left i/p to join
     * @param relationName access hfapfile for right i/p to join
     * @param outFilter    select expressions
     * @param rightFilter  reference to filter applied on right i/p
     * @param proj_list    shows what input fields go where in the output tuple
     * @param n_out_flds   number of outer relation fileds
     * @throws Exception
     * @throws UnknownKeyTypeException
     * @throws UnknowAttrType
     * @throws LowMemException
     * @throws PredEvalException
     * @throws TupleUtilsException
     * @throws PageNotReadException
     * @throws InvalidTypeException
     * @throws InvalidTupleSizeException
     * @throws IndexException
     * @throws JoinsException
     * @exception IESelfJoin2Exception exception from this class (To Implement)
     */
    public IESelfJoin2( AttrType in1[], int len_in1, short t1_str_sizes[], int amt_of_mem, String source, 
                        CondExpr outFilter[], FldSpec proj_list[], int n_out_flds) 
                        
                        throws JoinsException, IndexException,InvalidTupleSizeException, InvalidTypeException, 
                        PageNotReadException, TupleUtilsException,PredEvalException, LowMemException, UnknowAttrType, 
                        UnknownKeyTypeException, Exception {

        // Index set for getNext()
        first = true;
        visited = false;

        // Set Ascending based on Op
        TupleOrder[] sortingDir = SetSortingDirection(outFilter);

        // Set offset if strict inequality is present
        Off = SetOffset(outFilter);

        //Copy Required Fields in New heapfile to be used as source
        sourceFile = new Heapfile(source);
        Scan sourceScan = sourceFile.openScan();
        Tuple tuple = null;
        RID rid = new RID();

        AttrType[] types = new AttrType[4];
        types[0] = new AttrType(AttrType.attrInteger);
        types[1] = new AttrType(AttrType.attrInteger);
        types[2] = new AttrType(AttrType.attrInteger);
        types[3] = new AttrType(AttrType.attrInteger);

        short[] strSizes =  new short[1];
        strSizes[0] = 0;

        Heapfile file = new Heapfile("file.tmp");

        while((tuple = sourceScan.getNext(rid))!=null){
            tuple.setHdr((short) len_in1, in1, t1_str_sizes);

            Tuple tmp = new Tuple();
            tmp.setHdr((short) types.length, types, strSizes);

            tmp.setIntFld(1, rid.pageNo.pid);
            tmp.setIntFld(2, rid.slotNo);
            tmp.setIntFld(3, tuple.getIntFld(outFilter[0].operand1.symbol.offset));
            tmp.setIntFld(4, tuple.getIntFld(outFilter[1].operand1.symbol.offset));

            file.insertRecord(tmp.getTupleByteArray());
        }
        
        FldSpec[] Sprojection = new FldSpec[types.length];
        for (int i = 0; i < types.length; i++) {
            Sprojection[i] = new FldSpec(new RelSpec(RelSpec.outer), i + 1);
        }


        //Create Iterator To be used for sort
        FileScan am1 = new FileScan("file.tmp", types, strSizes, (short) types.length, (short) types.length, Sprojection, null);   
        FileScan am2 = new FileScan("file.tmp", types, strSizes, (short) types.length, (short) types.length, Sprojection, null);      

        // Creation Array 1
        Iterator L1 = new Sort( types, (short) types.length, strSizes, am1, 3, sortingDir[0], strSizes[0], amt_of_mem);

        sorted1 = new ArrayList<RID>();
        tuple = null;
        while ((tuple = L1.get_next()) != null) {
            //tuple.print(types);
            PageId pageId = new PageId( tuple.getIntFld(1));
            int slotNo = tuple.getIntFld(2);
            RID tmp = new RID(pageId,slotNo);
            sorted1.add(tmp);
        }


        // L2 array created
        // Check relation of amt_of_mem wrt to other things.... (WIP)
        Iterator L2 = new Sort( types, (short) types.length, strSizes, am2, 4, sortingDir[1], strSizes[0], amt_of_mem);

        sorted2 = new ArrayList<RID>();
        tuple = null;
        while ((tuple = L2.get_next()) != null) {
            //tuple.print(types);
            PageId pageId = new PageId( tuple.getIntFld(1));
            int slotNo = tuple.getIntFld(2);
            RID tmp = new RID(pageId,slotNo);
            sorted2.add(tmp);
        }

        
        rel_len = sorted1.size();
        perm_arr = new int[rel_len];
        bitmap = new BitMap(rel_len, 10);


        for (int i = 0; i < rel_len; i++) {
            for (int j = 0; j < rel_len; j++ ){

                RID rid1 = sorted1.get(i);
                RID rid2 = sorted2.get(j);

                if( rid1.pageNo.pid == rid2.pageNo.pid && rid1.slotNo == rid2.slotNo){
                    perm_arr[i] = j;
                    break;
                }
            }
        }


        // Copy field types
        _in1 = new AttrType[in1.length];
        _t1StrSizes = new short[t1_str_sizes.length];
        System.arraycopy(in1, 0, _in1, 0, in1.length);
        System.arraycopy(t1_str_sizes, 0, _t1StrSizes, 0, t1_str_sizes.length);

        // Access oterator
        Jtuple = new Tuple();

        AttrType[] Jtypes = new AttrType[n_out_flds];
        short[] t_size;

        perm_mat = proj_list;
        nOutFlds = n_out_flds;

        try {
            t_size = TupleUtils.setup_op_tuple(Jtuple, Jtypes, in1, len_in1, in1, len_in1, t1_str_sizes, t1_str_sizes, proj_list, nOutFlds);
        } catch (TupleUtilsException e) {
            // throw new IESelfJoin2Exception(e, "TupleUtilsException is caught by
            // IESelfJoin2.java");
        }


        //System.out.println("Permutation Array:" + Arrays.toString(perm_arr));

        file.deleteFile();
    }

    private TupleOrder[] SetSortingDirection(CondExpr outFilter[]) {
        TupleOrder[] sortingDir = new TupleOrder[outFilter.length - 1];

        for (int i = 0; i < (outFilter.length - 1); i++) {
            String op = outFilter[i].op.toString();

            switch (op) {

                case "aopLT":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Descending : TupleOrder.Ascending);
                    break;

                case "aopLE":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Descending : TupleOrder.Ascending);
                    break;

                case "aopGT":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Ascending : TupleOrder.Descending);
                    break;

                case "aopGE":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Ascending : TupleOrder.Descending);
                    break;

                default:
                    System.out.println("The FilterOut contains the " + op + " operator");
                    break;
            }
        }

        return sortingDir;
    }

    private short SetOffset(CondExpr outFilter[]) {
        short offset = 0;

        for (int i = 0; i < (outFilter.length - 1); i++) {
            String op = outFilter[i].op.toString();

            switch (op) {

                case "aopLT":
                    offset = 1;
                    break;

                case "aopGT":
                    offset = 1;
                    break;

                default:
                    break;
            }
        }

        return offset;
    }

    /**
     * @exception PageNotReadException    exception from lower layer
     * @exception TupleUtilsException     exception from using tuple utilities
     * @exception PredEvalException       exception from PredEval class
     * @exception SortException           sort exception
     * @exception LowMemException         memory error
     * @exception UnknowAttrType          attribute type unknown
     * @exception UnknownKeyTypeException key type unknown
     * @exception Exception               other exceptions
     * 
     */
    public Tuple get_next() throws IOException, JoinsException, IndexException, InvalidTupleSizeException,
            InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, SortException,
            LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception {

        if (first){
            first = false;
            visited = false;
            L2pos = 0;
        }
        
        while (L2pos < rel_len) {
            if (visited == false) {
                visited = true;
                L1pos = perm_arr[L2pos];
                bitmap.setBit(L1pos);
                bit_out = bitmap.getValidPositions(L1pos + Off);
                outerTuple = sourceFile.getRecord(sorted1.get(L1pos));
                outerTuple.setHdr((short)_in1.length, _in1, _t1StrSizes);
            }

            if (bit_out.hasNext()){
                int i = bit_out.next();
                Tuple innerTuple = sourceFile.getRecord(sorted1.get(i));
                innerTuple.setHdr((short)_in1.length, _in1, _t1StrSizes);
                Projection.Join(innerTuple, _in1, outerTuple, _in1, Jtuple, perm_mat, nOutFlds);
                return Jtuple;
            }

            L2pos++;
            visited = false;
        }

        return null;
    }

    /**
     * implement the abstract method close() from super class Iterator to finish
     * cleaning up
     * 
     * @exception IOException    I/O error from lower layers
     * @exception JoinsException join error from lower layers
     * @exception IndexException index access error
     */
    public void close() throws JoinsException, IOException, IndexException {
        if (!closeFlag) {

            try {
                
            } catch (Exception e) {
                throw new JoinsException(e, "IESelfJoin2.java: error in closing iterator.");
            }
            closeFlag = true;
        }
    }

}
