package iterator;

import heap.*;
import global.*;
import bufmgr.*;
import diskmgr.*;
import index.*;
import java.lang.*;
import java.net.SocketImpl;
import java.io.*;

import java.util.*;

/**
 *
 * This file contains an implementation of the nested loops join algorithm as
 * described in the Shapiro paper. The algorithm is extremely simple:
 *
 * foreach tuple r in R do foreach tuple s in S do if (ri == sj) then add (r, s)
 * to the result.
 */

public class IESelfJoin extends Iterator {
  private AttrType _in1[];
  private short[] _t1_str_sizes;
  private Iterator outer;
  private Tuple Jtuple; // Joined tuple
  private FldSpec perm_mat[];
  private int nOutFlds;

  private int rel_len;
  private short eqOff;
  private boolean visited;
  private int i,j;

  Heapfile tmpFile;
  Tuple outerTuple;
  private ArrayList<RID> RIDList;
  Scan fileScan;

  /**
   * constructor Initialize the two relations which are joined, including relation
   * type,
   * 
   * @param in1          Array containing field types of R.
   * @param len_in1      # of columns in R.
   * @param t1_str_sizes shows the length of the string fields.
   * @param in2          Array containing field types of S
   * @param len_in2      # of columns in S
   * @param t2_str_sizes shows the length of the string fields.
   * @param amt_of_mem   IN PAGES
   * @param am1          access method for left i/p to join
   * @param relationName access hfapfile for right i/p to join
   * @param outFilter    select expressions
   * @param rightFilter  reference to filter applied on right i/p
   * @param proj_list    shows what input fields go where in the output tuple
   * @param n_out_flds   number of outer relation fileds
   * @throws Exception
   * @throws UnknownKeyTypeException
   * @throws UnknowAttrType
   * @throws LowMemException
   * @throws PredEvalException
   * @throws TupleUtilsException
   * @throws PageNotReadException
   * @throws InvalidTypeException
   * @throws InvalidTupleSizeException
   * @throws IndexException
   * @throws JoinsException
   * @exception IESelfJoinException exception from this class (To Implement)
   */
  public IESelfJoin(AttrType in1[], int len_in1, short t1_str_sizes[], int amt_of_mem, Iterator am1, CondExpr outFilter[], CondExpr rightFilter[], FldSpec proj_list[],
      int n_out_flds)
      throws JoinsException, IndexException, InvalidTupleSizeException, InvalidTypeException, PageNotReadException,
      TupleUtilsException, PredEvalException, LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception {


    _in1 = in1;
    _t1_str_sizes = t1_str_sizes;

    // Set offset if strict inequality is present
    eqOff = SetOffset(outFilter);

    // Set Ascending based on Op
    TupleOrder[] sortingDir = SetSortingDirection(outFilter);

    // L1 array created
    Tuple tuple = null;

    // Check relation of amt_of_mem wrt to other things.... (WIP)
    Iterator L1 = new Sort(in1, (short) len_in1, t1_str_sizes, am1, outFilter[0].operand1.symbol.offset, sortingDir[0], 0, amt_of_mem);

    tmpFile = new Heapfile(null);
    RIDList = new ArrayList<RID>();
    while ((tuple = L1.get_next()) != null) {
      RIDList.add(tmpFile.insertRecord(tuple.getTupleByteArray()));
    }
    rel_len = RIDList.size();


    perm_mat = proj_list;
    nOutFlds = n_out_flds;

    Jtuple = new Tuple();
    AttrType[] Jtypes = new AttrType[n_out_flds];
    short[] t_size;

    try {
      t_size = TupleUtils.setup_op_tuple(Jtuple, Jtypes, in1, len_in1, in1, len_in1, t1_str_sizes, t1_str_sizes, proj_list, nOutFlds);
    } catch (TupleUtilsException e) {
      // throw new IESelfJoinException(e, "TupleUtilsException is caught by IESelfJoin.java");
    }

    i=0;
    j=0;
    visited = true;
    fileScan = tmpFile.openScan();
  }

  private TupleOrder[] SetSortingDirection( CondExpr outFilter[]){
    TupleOrder[] sortingDir = new TupleOrder[outFilter.length-1];

    for( int i = 0; i < (outFilter.length-1); i++){
      String op = outFilter[i].op.toString();

      switch(op){

        case "aopLT":
          sortingDir[i] = new TupleOrder( i%2==0 ? TupleOrder.Descending : TupleOrder.Ascending);
          break;

        case "aopLE":
          sortingDir[i] = new TupleOrder( i%2==0 ? TupleOrder.Descending : TupleOrder.Ascending);
          break;

        case "aopGT":
          sortingDir[i] = new TupleOrder( i%2==0 ? TupleOrder.Ascending : TupleOrder.Descending);
          break;

        case "aopGE":
          sortingDir[i] = new TupleOrder( i%2==0 ? TupleOrder.Ascending : TupleOrder.Descending);
          break;

        default:
          System.out.println("The FilterOut contains the " + op + " operator");
          break;
      }
    }

    return sortingDir;
  }

  private short SetOffset( CondExpr outFilter[]){
    short offset = 0;

    for( int i = 0; i < (outFilter.length-1); i++){
      String op = outFilter[i].op.toString();

      switch(op){

        case "aopLT":
          offset = 1;
          break;

        case "aopGT":
          offset = 1;
          break;

        default:
          break;
      }
    }

    return offset;
  }



  /**
   * @exception PageNotReadException    exception from lower layer
   * @exception TupleUtilsException     exception from using tuple utilities
   * @exception PredEvalException       exception from PredEval class
   * @exception SortException           sort exception
   * @exception LowMemException         memory error
   * @exception UnknowAttrType          attribute type unknown
   * @exception UnknownKeyTypeException key type unknown
   * @exception Exception               other exceptions
   * 
   */
  public Tuple get_next() throws IOException, JoinsException, IndexException, InvalidTupleSizeException,
      InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, SortException,
      LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception {


    while( i < rel_len){
      if( visited ){
        outerTuple = tmpFile.getRecord(RIDList.get(i));
        outerTuple.setHdr((short)_in1.length, _in1, _t1_str_sizes);
        j = i + eqOff;
        visited = false;
      }

      while( j < rel_len){
        Tuple innerTuple = tmpFile.getRecord(RIDList.get(j));
        innerTuple.setHdr((short)_in1.length, _in1, _t1_str_sizes);

        Projection.Join(outerTuple, _in1, 
                        innerTuple, _in1, 
                        Jtuple, perm_mat, nOutFlds);
        j++;
        return Jtuple;  
      }

      visited = true;
      i++;
    }

    return null;
  }

  /**
   * implement the abstract method close() from super class Iterator to finish
   * cleaning up
   * 
   * @exception IOException    I/O error from lower layers
   * @exception JoinsException join error from lower layers
   * @exception IndexException index access error
   */
  public void close() throws JoinsException, IOException, IndexException {
    if (!closeFlag) {

      try {
        System.out.println("TmpFile Deleted");
        // tmpFile.deleteFile(); Gives Problem on close
      } catch (Exception e) {
        throw new JoinsException(e, "IESelfJoin.java: error in closing iterator.");
      }
      closeFlag = true;
    }
  }
}

