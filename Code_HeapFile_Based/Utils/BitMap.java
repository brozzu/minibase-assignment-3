package iterator;
import java.util.ArrayList;
import java.util.Iterator;

public class BitMap{

  int[] bitmap;
  int[] lookup;
  int scale_factor;

  public BitMap(int len, int _scale_factor){
    bitmap = new int[len];
    scale_factor = _scale_factor;
    lookup = new int[ (int) Math.ceil( ((double) len )/ scale_factor)];
  }

  public boolean setBit(int pos){
    if( pos >= bitmap.length){
      return false;
    }

    bitmap[pos] = 1;
    lookup[(int) Math.floor( ((double)pos) / scale_factor)] = 1;

    return true;
  }

  public int getBit(int pos){
    if( pos >= bitmap.length){
      return -1;
    }

    return bitmap[pos];
  }

  public Iterator<Integer> getValidPositions(int pos){
    ArrayList<Integer> valid = new ArrayList<Integer>();

    for( int i= pos/scale_factor; i < lookup.length; i++){
      if( lookup[i] == 1 ){
        for ( int j = i * scale_factor; j<bitmap.length && j < (i+1)*scale_factor; j++){
          if( j >= pos && bitmap[j] == 1){
            valid.add(j);
          }
        }
      }
    }

    
    return valid.iterator();
  }


  public void print_bitmap(){
    
    System.out.println("Lookup:\n" + lookup.toString());
    System.out.println("BitMap:\n" + bitmap.toString());
  }
}
