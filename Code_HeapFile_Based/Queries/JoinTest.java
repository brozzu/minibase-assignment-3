package tests2;

import iterator.*;
import heap.*;
import global.*;
import index.*;
import java.io.*;
import java.util.*;
import java.lang.*;
import diskmgr.*;
import bufmgr.*;
import btree.*;
import catalog.*;

import tests.JoinTest_util;


class JoinsDriver implements GlobalConst {

    /**
     * Constructor
     */

    private String times_out_file;
    private int n_tuples;
    private int amt_of_mem;

    public JoinsDriver(int _n_tuples, int _amt_of_mem, String _times_out_file){

        String dbpath = "/tmp/" + System.getProperty("user.name") + ".minibase.jointestdb";
        String logpath = "/tmp/" + System.getProperty("user.name") + ".joinlog";

        String remove_cmd = "/bin/rm -rf ";
        String remove_logcmd = remove_cmd + logpath;
        String remove_dbcmd = remove_cmd + dbpath;
        String remove_joincmd = remove_cmd + dbpath;

        times_out_file = _times_out_file;
        n_tuples = _n_tuples;
        amt_of_mem = _amt_of_mem;

        try {
            Runtime.getRuntime().exec(remove_logcmd);
            Runtime.getRuntime().exec(remove_dbcmd);
            Runtime.getRuntime().exec(remove_joincmd);
        } catch (IOException e) {
            System.err.println("" + e);
        }

        /*
         * ExtendedSystemDefs extSysDef = new ExtendedSystemDefs(
         * "/tmp/minibase.jointestdb", "/tmp/joinlog", 1000,500,200,"Clock");
         */

        SystemDefs sysdef = new SystemDefs(dbpath, 1000000, NUMBUF, "Clock");

        JoinTest_util.File2Heap("//home/laod/EURECOM/DBSys/minibase-assignment-3/src/R.txt", "R.in", n_tuples);
        JoinTest_util.File2Heap("/home/laod/EURECOM/DBSys/minibase-assignment-3/src/S.txt", "S.in", n_tuples);
        JoinTest_util.File2Heap("/home/laod/EURECOM/DBSys/minibase-assignment-3/src/Q.txt", "Q.in", n_tuples);
    }

    public boolean runTests() throws IOException {
        //Query1a curr_query = new Query1a();
        //Query1b qcuey1b = new Query1b();
        //Query2a query2a = new Query2a();
        //Query2b query2b = new Query2b();
        //Query2c query2c = new Query2c();
        //Query2c_1 query2c_1 = new Query2c_1();
        //Query2c_2 query2c_2 = new Query2c_2();
        long[] startTime = new long[10];
        long[] endTime = new long[10];
        long[] duration = new long[10];

        try {
            
            startTime[0] = System.nanoTime();
            Query2a query2a = new Query2a(amt_of_mem);
            endTime[0] = System.nanoTime();

            startTime[1] = System.nanoTime();
            Query2a_nlj query2a_nlj = new Query2a_nlj(amt_of_mem);
            endTime[1] = System.nanoTime();
            
            startTime[2] = System.nanoTime();
            Query2b query2b = new Query2b(amt_of_mem);
            endTime[2] = System.nanoTime();

            startTime[3] = System.nanoTime();
            Query2b_nlj query2b_nlj = new Query2b_nlj(amt_of_mem);
            endTime[3] = System.nanoTime();
            
            startTime[4] = System.nanoTime();
            Query2c query2c = new Query2c(amt_of_mem);
            endTime[4] = System.nanoTime();

            startTime[5] = System.nanoTime();
            Query2c_nlj query2c_nlj = new Query2c_nlj(amt_of_mem);
            endTime[5] = System.nanoTime();
        
            startTime[6] = System.nanoTime();
            Query2c_1 query2c_1 = new Query2c_1(amt_of_mem);
            endTime[6] = System.nanoTime();

            startTime[7] = System.nanoTime();
            Query2c_1_nlj query2c_1_nlj = new Query2c_1_nlj(amt_of_mem);
            endTime[7] = System.nanoTime();
            
            startTime[8] = System.nanoTime();
            Query2c_2 query2c_2 = new Query2c_2(amt_of_mem);
            endTime[8] = System.nanoTime();

            startTime[9] = System.nanoTime();
            Query2c_2_nlj query2c_2_nlj = new Query2c_2_nlj(amt_of_mem);
            endTime[9] = System.nanoTime();
        }
        catch (Exception e){
            return false;
        }
        for (int i=0; i<duration.length; i++){
            duration[i] = endTime[i]-startTime[i];
        }

        tests.JoinTest_util.Time2File(duration, n_tuples, times_out_file);

        return true;
    }

}

public class JoinTest {
    public static void main(String argv[]) throws IOException {
        boolean sortstatus;
        // SystemDefs global = new SystemDefs("bingjiedb", 100, 70, null);
        // JavabaseDB.openDB("/tmp/nwangdb", 5000);

        FileWriter myWriter = new FileWriter("times.csv");
        myWriter.write("n_tuples, Query2a, Query2a_nlj, Query2b, Query2b_nlj, Query2c, Query2c_nlj, Query2c_1, Query2c_1_nlj, Query2c_2, Query2c_2_nlj\n");
        myWriter.close();

        sortstatus = true;
        int amt_of_mem = 100;

        for (int i=100; i<=100; i*=2){
            try{
                JoinsDriver jjoin = new JoinsDriver(i, amt_of_mem, "times.csv");
                sortstatus = jjoin.runTests();
                if (sortstatus == false){
                    System.out.println("Something went wrong when processing n_tuples: " + i);
                }
            }
            catch(Exception e){
                System.out.println("Something wrong happened with n_tuples: " + i);
            }
            
            if (sortstatus != true) {
                System.out.println("Error ocurred during join tests");
            } else {
                System.out.println("join tests completed successfully");
            }
        }
        

        
    }
}
