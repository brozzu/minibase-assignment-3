package tests2;

import iterator.*;
import heap.*;
import global.*;
import index.*;
import java.io.*;
import java.util.*;
import java.lang.*;
import diskmgr.*;
import bufmgr.*;
import btree.*;
import catalog.*;

import tests.JoinTest_util;

class Query1a implements GlobalConst, GlobalConst_test {
    public Query1a(int amt_of_mem) {
 
        System.out.print("**********************Query1a strating *********************\n");
        boolean status = OK;

        // Sailors, Boats, Reserves Queries.
        System.out.print("Query: SELECT R.1 S.1 \n"
            + "  FROM   R, S\n" 
            + "  WHERE  R.3 < S.3\n\n");

        System.out.print("\n(Tests FileScan, Projection, and Sort-Merge Join)\n");

        CondExpr[] outFilter = new CondExpr[2];
        outFilter[0] = new CondExpr();
        outFilter[1] = new CondExpr();

        Query1a_CondExpr(outFilter);

        Tuple t = new Tuple();

        AttrType[] Stypes = new AttrType[4];
        Stypes[0] = new AttrType(AttrType.attrInteger);
        Stypes[1] = new AttrType(AttrType.attrInteger);
        Stypes[2] = new AttrType(AttrType.attrInteger);
        Stypes[3] = new AttrType(AttrType.attrInteger);

        // SOS
        short[] Ssizes = null;

        AttrType[] Rtypes = new AttrType[4];
        Rtypes[0] = new AttrType(AttrType.attrInteger);
        Rtypes[1] = new AttrType(AttrType.attrInteger);
        Rtypes[2] = new AttrType(AttrType.attrInteger);
        Rtypes[3] = new AttrType(AttrType.attrInteger);

        short[] Rsizes = null;
        FldSpec[] Rprojection = new FldSpec[4];
        Rprojection[0] = new FldSpec(new RelSpec(RelSpec.outer), 1);
        Rprojection[1] = new FldSpec(new RelSpec(RelSpec.outer), 2);
        Rprojection[2] = new FldSpec(new RelSpec(RelSpec.outer), 3);
        Rprojection[3] = new FldSpec(new RelSpec(RelSpec.outer), 4);

        FileScan am = null;
        try {
            am = new FileScan("R.in", Rtypes, Rsizes, (short) 4, (short) 4, Rprojection, null);
        } catch (Exception e) {
            status = FAIL;
            System.err.println("" + e);
            e.printStackTrace();
        }

        if (status != OK) {
            // bail out
            System.err.println("*** Error setting up scan for sailors");
            Runtime.getRuntime().exit(1);
        }

        FldSpec[] proj_list = new FldSpec[2];
        proj_list[0] = new FldSpec(new RelSpec(RelSpec.outer), 1);
        proj_list[1] = new FldSpec(new RelSpec(RelSpec.innerRel), 1);

        AttrType[] jtype = new AttrType[2];
        jtype[0] = new AttrType(AttrType.attrInteger);
        jtype[1] = new AttrType(AttrType.attrInteger);

        TupleOrder ascending = new TupleOrder(TupleOrder.Ascending);
        NestedLoopsJoins sm = null;
        try {
            sm = new NestedLoopsJoins(Rtypes, 4, Rsizes, Stypes, 4, Ssizes, amt_of_mem, am, "S.in", outFilter, null, proj_list, 2); 
        } catch (Exception e) {
            System.err.println("*** join error in SortMerge constructor ***");
            status = FAIL;
            System.err.println("" + e);
            e.printStackTrace();
        }

        if (status != OK) {
            // bail out
            System.err.println("*** Error constructing SortMerge");
            Runtime.getRuntime().exit(1);
        }

        //QueryCheck qcheck1 = new QueryCheck(1);

        t = null;

        try {

            tests.JoinTest_util.Iterator2File(sm, "query1aNLJ.csv");

            while ((t = sm.get_next()) != null) {
                //t.print(jtype);

                //qcheck1.Check(t);
            }
        } catch (Exception e) {
            System.err.println("" + e);
            e.printStackTrace();
            status = FAIL;
        }
        if (status != OK) {
            // bail out
            System.err.println("*** Error in get next tuple ");
            Runtime.getRuntime().exit(1);
        }

        try {
            sm.close();
        } catch (Exception e) {
            status = FAIL;
            e.printStackTrace();
        }
        System.out.println("\n");
        if (status != OK) {
            // bail out
            System.err.println("*** Error in closing ");
            Runtime.getRuntime().exit(1);
        }
    }

    private void Query1a_CondExpr(CondExpr[] expr) {

        expr[0].next = null;
        expr[0].op = new AttrOperator(AttrOperator.aopLT);
        expr[0].type1 = new AttrType(AttrType.attrSymbol);
        expr[0].type2 = new AttrType(AttrType.attrSymbol);
        expr[0].operand1.symbol = new FldSpec(new RelSpec(RelSpec.outer), 3);
        expr[0].operand2.symbol = new FldSpec(new RelSpec(RelSpec.innerRel), 3);

        expr[1] = null;
    }

}
