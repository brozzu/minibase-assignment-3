package tests;

import heap.*;
import global.*;
import java.io.*;
import iterator.*;

public class JoinTest_util {
    public static boolean File2Heap(String fileNameInput, String fileNameOutput, int max_num_tuples) {

        /**
         * 
         * BUILD table from "fileInput"
         * 
         * @parameter fileNameInput Name of file containing data
         * @parameter fileNameOutput Name of table saved in the DB
         * @parameter max_num_tuples Max number of tuple to load from the file in case
         *            ofbig files.
         * 
         **/

        if (fileNameInput == null || fileNameOutput == null) {
            return false;
        }

        if (max_num_tuples <= 0) {
            max_num_tuples = Integer.MAX_VALUE; // Load tuples until the HeapFile can contain them
        }
        /* Create relation */

        AttrType[] types = new AttrType[4];
        types[0] = new AttrType(AttrType.attrInteger);
        types[1] = new AttrType(AttrType.attrInteger);
        types[2] = new AttrType(AttrType.attrInteger);
        types[3] = new AttrType(AttrType.attrInteger);

        short numField = 4;

        Tuple t = new Tuple();

        try {
            t.setHdr(numField, types, null);
        } catch (Exception e) {

            System.err.println("*** error in Tuple.setHdr() ***");
            e.printStackTrace();
            return false;
        }

        int t_size = t.size();

        RID rid;

        Heapfile f = null;

        try {
            f = new Heapfile(fileNameOutput);
        }

        catch (Exception e) {
            System.err.println("*** error in Heapfile constructor ***");
            e.printStackTrace();
            return false;
        }

        t = new Tuple(t_size);

        try {
            t.setHdr((short) 4, types, null);
        } catch (Exception e) {
            System.err.println("*** error in Tuple.setHdr() ***");
            e.printStackTrace();
            return false;
        }

        int cont = 0; // To limit the size of table

        try {

            File file = new File(fileNameInput);
            BufferedReader reader = null;
            reader = new BufferedReader(new FileReader(file));

            String text = null;
            text = reader.readLine(); // To skip header
            text = "";

            while ((text = reader.readLine()) != null && cont != max_num_tuples) {

                String[] attributes = text.split(",");
                t.setIntFld(1, Integer.parseInt(attributes[0]));
                t.setIntFld(2, Integer.parseInt(attributes[1]));
                t.setIntFld(3, Integer.parseInt(attributes[2]));
                t.setIntFld(4, Integer.parseInt(attributes[3]));
                f.insertRecord(t.getTupleByteArray());
                cont++;
            }
            reader.close();
        } catch (FileNotFoundException e1) {
            System.err.println("*** File " + fileNameInput + " ***");
            e1.printStackTrace();
            return false;
        } catch (Exception e) {

            System.err.println("*** Heapfile error in Tuple.setIntFld() ***");
            e.printStackTrace();
            return false;

        }

        System.out.println("Number of tuple inserted: " + cont);
        return true;

    }

    public static String[][] File2List(String myfile) {

        BufferedReader abc;
        try {
            abc = new BufferedReader(new FileReader(myfile));

            int M = (int) abc.lines().count();
            abc.close();

            int i = 0;

            abc = new BufferedReader(new FileReader(myfile));
            String[][] east;
            String line;
            line = abc.readLine() + "," + Integer.toString(0);

            String[] dimes = line.split(",");

            int N = dimes.length;

            east = new String[M][N];

            east[0] = dimes;

            for (i = 1; i < M; i++) {
                line = abc.readLine();
                dimes = line.split(",");
                east[i] = dimes;
            }
            abc.close();

            return east;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

    }

    public static boolean Time2File(long[] times, int n_tuples, String nameOutFile) throws IOException {
        if (nameOutFile == null) {
            return false;
        }

        FileWriter myWriter = new FileWriter(nameOutFile, true);


        /* Create relation */
        

        try{
            myWriter.write("" + n_tuples);
            for (int i = 0; i<times.length; i++){
                myWriter.write("," + times[i]);
            }
            myWriter.write("\n");
            myWriter.close();
        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static boolean Iterator2File(Iterator itr, String nameOutFile, AttrType[] types) 
    throws IOException, UnknowAttrType, LowMemException, JoinsException, Exception {
        if (nameOutFile == null) {
            return false;
        }

        FileWriter myWriter = new FileWriter(nameOutFile);


        for( int i=0; i< types.length; i++){
            myWriter.write("Field" + i );
            if( i == types.length - 1){
                myWriter.write("\n");
            }else{
                myWriter.write(",");
            }
        }


        Tuple t = new Tuple();
        t = null;
        try{
            while ((t = itr.get_next()) != null) {
                myWriter.write(Tuple2CSV(t,types) + "\n");
            }

            myWriter.close();
        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static boolean Iterator2Null(Iterator itr) 
    throws IOException, UnknowAttrType, LowMemException, JoinsException, Exception {

        /* Create relation */
        AttrType[] types = new AttrType[2];
        types[0] = new AttrType(AttrType.attrInteger);
        types[1] = new AttrType(AttrType.attrInteger);

        Tuple t = new Tuple();
        t = null;
        try{
            while ((t = itr.get_next()) != null) {
            }
        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }

        return true;
    }


    public static String Tuple2CSV(Tuple t, AttrType type[])
    throws IOException, FieldNumberOutOfBoundException {
    int i, val;
    float fval;
    String sval;

    String curr = "";
    for (i=0; i< type.length; i++){
        switch(type[i].attrType) {

        case AttrType.attrInteger:
            val = t.getIntFld(i+1);
            curr = curr + val + ",";
            break;

        case AttrType.attrReal:
            fval = t.getFloFld(i+1);
            curr = curr + fval + ",";
            break;

        case AttrType.attrString:
            sval = t.getStrFld(i+1);
            curr = curr + sval + ",";
            break;
        
        case AttrType.attrNull:
        case AttrType.attrSymbol:
            break;
        }
    }

    return curr.substring(0, curr.length() - 1);
    }
}
