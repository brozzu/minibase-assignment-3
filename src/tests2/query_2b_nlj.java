package tests2;
//originally from : joins.C

import iterator.*;
import heap.*;
import global.*;
import index.*;
import java.io.*;
import java.util.*;
import java.lang.*;
import diskmgr.*;
import bufmgr.*;
import btree.*;
import catalog.*;

import tests.JoinTest_util;

class Query2b_nlj implements GlobalConst {
    private boolean OK = true;
    private boolean FAIL = false;

    public Query2b_nlj(int amt_of_mem) {

        System.out.print("**********************Query2b_nlj strating *********************\n");
        boolean status = OK;

        // Sailors, Boats, Reserves Queries.
        System.out.print("Query: SELECT Q_1.1 Q_2.1 \n" 
                + " FROM   Q AS Q_1, Q AS Q_2\n"
                + "  WHERE  Q_1.3 <= Q_2.3 AND Q_1.4 < Q_1.4\n\n");

        System.out.print("\n(Tests FileScan, Projection, and Sort-Merge Join)\n");

        CondExpr[] outFilter = new CondExpr[3];
        outFilter[0] = new CondExpr();
        outFilter[1] = new CondExpr();
        outFilter[2] = new CondExpr();

        Query2b_nlj_CondExpr(outFilter);

        Tuple t = new Tuple();

        AttrType[] Stypes = new AttrType[4];
        Stypes[0] = new AttrType(AttrType.attrInteger);
        Stypes[1] = new AttrType(AttrType.attrInteger);
        Stypes[2] = new AttrType(AttrType.attrInteger);
        Stypes[3] = new AttrType(AttrType.attrInteger);

        short[] Ssizes = new short[1];
        Ssizes[0] = 0;

        FldSpec[] Sprojection = new FldSpec[4];
        Sprojection[0] = new FldSpec(new RelSpec(RelSpec.outer), 1);
        Sprojection[1] = new FldSpec(new RelSpec(RelSpec.outer), 2);
        Sprojection[2] = new FldSpec(new RelSpec(RelSpec.outer), 3);
        Sprojection[3] = new FldSpec(new RelSpec(RelSpec.outer), 4);

        FileScan am = null;
        try {
            am = new FileScan("Q.in", Stypes, Ssizes, (short) 4, (short) 4, Sprojection, null);
        } catch (Exception e) {
            status = FAIL;
            System.err.println("" + e);
        }

        if (status != OK) {
            // bail out
            System.err.println("*** Error setting up scan for sailors");
            Runtime.getRuntime().exit(1);
        }


        FldSpec[] proj_list = new FldSpec[2];
        proj_list[0] = new FldSpec(new RelSpec(RelSpec.outer), 1);
        proj_list[1] = new FldSpec(new RelSpec(RelSpec.innerRel), 1);

        AttrType[] jtype = new AttrType[2];
        jtype[0] = new AttrType(AttrType.attrInteger);
        jtype[1] = new AttrType(AttrType.attrInteger);

        NestedLoopsJoins sm = null;
        try {
            sm = new NestedLoopsJoins(Stypes, 4, Ssizes, Stypes, 4, Ssizes, amt_of_mem, am, "Q.in", outFilter, null, proj_list, 2); 
        } catch (Exception e) {
            System.err.println("*** join error in SortMerge constructor ***");
            status = FAIL;
            System.err.println("" + e);
            e.printStackTrace();
        }

        if (status != OK) {
            // bail out
            System.err.println("*** Error constructing SortMerge");
            Runtime.getRuntime().exit(1);
        }

        t = null;

        try {
            tests.JoinTest_util.Iterator2File(sm, "query2bNLJ.csv", jtype);
            //tests.JoinTest_util.Iterator2Null(sm);
        } catch (Exception e) {
            System.err.println("" + e);
            e.printStackTrace();
            status = FAIL;
        }
        if (status != OK) {
            // bail out
            System.err.println("*** Error in get next tuple ");
            Runtime.getRuntime().exit(1);
        }

        try {
            sm.close();
        } catch (Exception e) {
            status = FAIL;
            e.printStackTrace();
        }
        System.out.println("\n");
        if (status != OK) {
            // bail out
            System.err.println("*** Error in closing ");
            Runtime.getRuntime().exit(1);
        }
    }

    private void Query2b_nlj_CondExpr(CondExpr[] expr) {
        expr[0].next = null;
        expr[0].op = new AttrOperator(AttrOperator.aopLE);
        expr[0].type1 = new AttrType(AttrType.attrSymbol);
        expr[0].type2 = new AttrType(AttrType.attrSymbol);
        expr[0].operand1.symbol = new FldSpec(new RelSpec(RelSpec.outer), 3);
        expr[0].operand2.symbol = new FldSpec(new RelSpec(RelSpec.innerRel), 3);

        expr[1].next = null;
        expr[1].op = new AttrOperator(AttrOperator.aopLT);
        expr[1].type1 = new AttrType(AttrType.attrSymbol);
        expr[1].type2 = new AttrType(AttrType.attrSymbol);
        expr[1].operand1.symbol = new FldSpec(new RelSpec(RelSpec.outer), 4);
        expr[1].operand2.symbol = new FldSpec(new RelSpec(RelSpec.innerRel), 4);

        expr[2] = null;
    }
}