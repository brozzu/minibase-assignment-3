package tests2;
//originally from : joins.C

import iterator.*;
import heap.*;
import global.*;
import index.*;
import java.io.*;
import java.util.*;
import java.lang.*;
import diskmgr.*;
import bufmgr.*;
import btree.*;
import catalog.*;

import tests.JoinTest_util;


class Query2c_2 implements GlobalConst{
    private boolean OK = true;
    private boolean FAIL = false;

    public Query2c_2(int amt_of_mem) {

        System.out.print("**********************Query2c_2 strating *********************\n");
        boolean status = OK;

        // Sailors, Boats, Reserves Queries.
        System.out.print("Query: SELECT R.1 Q.1 \n"
            + "  FROM   R, Q\n" 
            + "  WHERE  R.3 <= Q.3 AND R.4 < Q.4\n\n");

        CondExpr[] outFilter = new CondExpr[3];
        outFilter[0] = new CondExpr();
        outFilter[1] = new CondExpr();
        outFilter[2] = new CondExpr();

        Query_CondExpr(outFilter);

        Tuple t = new Tuple();

        //Both Tables have the same schema
        AttrType[] Stypes = new AttrType[4];
        Stypes[0] = new AttrType(AttrType.attrInteger);
        Stypes[1] = new AttrType(AttrType.attrInteger);
        Stypes[2] = new AttrType(AttrType.attrInteger);
        Stypes[3] = new AttrType(AttrType.attrInteger);

        short[] Ssizes = new short[1];
        Ssizes[0] = 0;

        FldSpec[] Sprojection = new FldSpec[4];
        Sprojection[0] = new FldSpec(new RelSpec(RelSpec.outer), 1);
        Sprojection[1] = new FldSpec(new RelSpec(RelSpec.outer), 2);
        Sprojection[2] = new FldSpec(new RelSpec(RelSpec.outer), 3);
        Sprojection[3] = new FldSpec(new RelSpec(RelSpec.outer), 4);

        FldSpec[] proj_list = new FldSpec[6];
        proj_list[0] = new FldSpec(new RelSpec(RelSpec.outer), 1);
        proj_list[1] = new FldSpec(new RelSpec(RelSpec.innerRel), 1);
        proj_list[2] = new FldSpec(new RelSpec(RelSpec.outer), 3);
        proj_list[3] = new FldSpec(new RelSpec(RelSpec.innerRel), 3);
        proj_list[4] = new FldSpec(new RelSpec(RelSpec.outer), 4);
        proj_list[5] = new FldSpec(new RelSpec(RelSpec.innerRel), 4);

        AttrType[] jtype = new AttrType[6];
        jtype[0] = new AttrType(AttrType.attrInteger);
        jtype[1] = new AttrType(AttrType.attrInteger);
        jtype[2] = new AttrType(AttrType.attrInteger);
        jtype[3] = new AttrType(AttrType.attrInteger);
        jtype[4] = new AttrType(AttrType.attrInteger);
        jtype[5] = new AttrType(AttrType.attrInteger);
        
        IEJoin sm = null;
        try {
            sm = new IEJoin(Stypes, 4, Ssizes, Stypes, 4, Ssizes, amt_of_mem, "R.in", "Q.in", outFilter, null, proj_list, proj_list.length);           
        } catch (Exception e) {
            System.err.println("*** join error in IEJoin constructor ***");
            status = FAIL;
            System.err.println("" + e);
            e.printStackTrace();
            return;
        }

        if (status != OK) {
        // bail out
        System.err.println("*** Error constructing SortMerge");
        Runtime.getRuntime().exit(1);
        }

        t = null;

        try {
            tests.JoinTest_util.Iterator2File(sm, "query2c_2IEJ.csv",jtype);
            //tests.JoinTest_util.Iterator2Null(sm);
        } catch (Exception e) {
            System.err.println("" + e);
            e.printStackTrace();
            status = FAIL;
        }
        if (status != OK) {
            // bail out
            System.err.println("*** Error in get next tuple ");
            Runtime.getRuntime().exit(1);
        }

        try {
            sm.close();
        } catch (Exception e) {
            status = FAIL;
            e.printStackTrace();
        }

        System.out.println("\n");
        if (status != OK) {
            // bail out
            System.err.println("*** Error in closing ");
            Runtime.getRuntime().exit(1);
        }
    }

    private void Query_CondExpr(CondExpr[] expr) {
        expr[0].next = null;
        expr[0].op = new AttrOperator(AttrOperator.aopLE);
        expr[0].type1 = new AttrType(AttrType.attrSymbol);
        expr[0].type2 = new AttrType(AttrType.attrSymbol);
        expr[0].operand1.symbol = new FldSpec(new RelSpec(RelSpec.outer), 3);
        expr[0].operand2.symbol = new FldSpec(new RelSpec(RelSpec.innerRel), 3);

        expr[1].next = null;
        expr[1].op = new AttrOperator(AttrOperator.aopLE);
        expr[1].type1 = new AttrType(AttrType.attrSymbol);
        expr[1].type2 = new AttrType(AttrType.attrSymbol);
        expr[1].operand1.symbol = new FldSpec(new RelSpec(RelSpec.outer), 4);
        expr[1].operand2.symbol = new FldSpec(new RelSpec(RelSpec.innerRel), 4);

        expr[2] = null;
    }
}