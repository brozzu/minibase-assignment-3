import pandas as pd

S = pd.read_csv("S.txt")
print(f"S original: {S.shape}")
print(f"S without duplicates: {S.drop_duplicates().shape}")
tmp =S.duplicated()
print([i for i in range(len(tmp)) if tmp[i]])
print([S.to_numpy()[i] for i in range(len(tmp)) if tmp[i]])

R = pd.read_csv("R.txt")
print(f"R original: {R.shape}")
print(f"R without duplicates: {R.drop_duplicates().shape}")
tmp =R.duplicated()
print([i for i in range(len(tmp)) if tmp[i]])
print([R.to_numpy()[i] for i in range(len(tmp)) if tmp[i]])

Q = pd.read_csv("Q.txt")
print(f"Q original: {Q.shape}")
print(f"Q without duplicates: {Q.drop_duplicates().shape}")
tmp = Q.duplicated()
print([i for i in range(len(tmp)) if tmp[i]])
print([Q.to_numpy()[i] for i in range(len(tmp)) if tmp[i]])
