package iterator;

import heap.*;
import global.*;
import bufmgr.*;
import diskmgr.*;
import index.*;
import java.lang.*;
import java.io.*;

import java.util.*;

/**
 *
 * This file contains an implementation of the nested loops join algorithm as
 * described in the Shapiro paper. The algorithm is extremely simple:
 *
 * foreach tuple r in R do foreach tuple s in S do if (ri == sj) then add (r, s)
 * to the result.
 */

public class IESelfJoin2 extends Iterator {
    private AttrType _in1[], _in2[];
    private Iterator outer;
    private Tuple Jtuple; // Joined tuple
    private FldSpec perm_mat[];
    private int nOutFlds;

    // Aggiunte da Brozzu
    private ArrayList<Tuple> list_1, list_2;
    private int[] perm_arr;
    private int rel_len;
    private BitMap bitmap;
    private boolean visited, first;
    private java.util.Iterator<Integer> bit_out;

    private int L1pos;
    private int L2pos;
    private short Off;

    /**
     * constructor Initialize the two relations which are joined, including relation
     * type,
     * 
     * @param in1          Array containing field types of R.
     * @param len_in1      # of columns in R.
     * @param t1_str_sizes shows the length of the string fields.
     * @param in2          Array containing field types of S
     * @param len_in2      # of columns in S
     * @param t2_str_sizes shows the length of the string fields.
     * @param amt_of_mem   IN PAGES
     * @param am1          access method for left i/p to join
     * @param relationName access hfapfile for right i/p to join
     * @param outFilter    select expressions
     * @param rightFilter  reference to filter applied on right i/p
     * @param proj_list    shows what input fields go where in the output tuple
     * @param n_out_flds   number of outer relation fileds
     * @throws Exception
     * @throws UnknownKeyTypeException
     * @throws UnknowAttrType
     * @throws LowMemException
     * @throws PredEvalException
     * @throws TupleUtilsException
     * @throws PageNotReadException
     * @throws InvalidTypeException
     * @throws InvalidTupleSizeException
     * @throws IndexException
     * @throws JoinsException
     * @exception IESelfJoin2Exception exception from this class (To Implement)
     */
    public IESelfJoin2(AttrType in1[], int len_in1, short t1_str_sizes[], AttrType in2[], int len_in2,
            short t2_str_sizes[], int amt_of_mem, Iterator am1, Iterator am2, CondExpr outFilter[],
            CondExpr rightFilter[], FldSpec proj_list[], int n_out_flds) throws JoinsException, IndexException,
            InvalidTupleSizeException, InvalidTypeException, PageNotReadException, TupleUtilsException,
            PredEvalException, LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception {

        // Index set for getNext()
        first = true;
        visited = false;

        // Set offset if strict inequality is present
        Off = SetOffset(outFilter);

        // Set Ascending based on Op
        TupleOrder[] sortingDir = SetSortingDirection(outFilter);

        Tuple tuple = new Tuple();

        // L1 array created
        // Check relation of amt_of_mem wrt to other things.... (WIP)
        Iterator L1 = new Sort(in1, (short) len_in1, t1_str_sizes, am1, outFilter[0].operand1.symbol.offset,
                sortingDir[0], t1_str_sizes[0], amt_of_mem);

        list_1 = new ArrayList<Tuple>();
        tuple = null;
        while ((tuple = L1.get_next()) != null) {
            Tuple x = new Tuple(tuple);
            list_1.add(x);
            //System.out.println("List 1 Item: " + x.getFloFld(4));
        }
        L1.close();
        System.out.println("List 1 Len: " + list_1.size());

        // L2 array created
        // Check relation of amt_of_mem wrt to other things.... (WIP)
        Iterator L2 = new Sort(in2, (short) len_in2, t2_str_sizes, am2, outFilter[1].operand1.symbol.offset,
                sortingDir[1], t2_str_sizes[0], amt_of_mem);

        list_2 = new ArrayList<Tuple>();
        tuple = null;
        while ((tuple = L2.get_next()) != null) {
            Tuple x = new Tuple(tuple);
            list_2.add(x);
            //System.out.println("List 2 Item: " + x.getIntFld(3));
        }
        System.out.println("List 2 Len: " + list_2.size());
        //L2.close();
        
        rel_len = list_1.size();
        perm_arr = new int[rel_len];
        bitmap = new BitMap(rel_len, 10);


        for (int i = 0; i < rel_len; i++) {
            for (int j = 0; j < rel_len; j++ ){
                boolean equal = true;

                //Compare each field
                for( int k = 0; k < in1.length; k++){
                    if( TupleUtils.CompareTupleWithTuple(in1[k], list_1.get(i), k+1 , list_2.get(j), k+1) != 0){
                        equal = false;
                    }
                }

                if(equal){
                    perm_arr[i] = j;
                }

            }
        }


        // Copy field types
        _in1 = new AttrType[in1.length];
        _in2 = new AttrType[in2.length];
        System.arraycopy(in1, 0, _in1, 0, in1.length);
        System.arraycopy(in2, 0, _in2, 0, in2.length);

        // Access oterator
        outer = am1;
        Jtuple = new Tuple();

        AttrType[] Jtypes = new AttrType[n_out_flds];
        short[] t_size;

        perm_mat = proj_list;
        nOutFlds = n_out_flds;

        try {
            t_size = TupleUtils.setup_op_tuple(Jtuple, Jtypes, in1, len_in1, in2, len_in2, t1_str_sizes, t2_str_sizes,
                    proj_list, nOutFlds);
        } catch (TupleUtilsException e) {
            // throw new IESelfJoin2Exception(e, "TupleUtilsException is caught by
            // IESelfJoin2.java");
        }


        //System.out.println("Permutation Array:" + Arrays.toString(perm_arr));

    }

    private TupleOrder[] SetSortingDirection(CondExpr outFilter[]) {
        TupleOrder[] sortingDir = new TupleOrder[outFilter.length - 1];

        for (int i = 0; i < (outFilter.length - 1); i++) {
            String op = outFilter[i].op.toString();

            switch (op) {

                case "aopLT":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Descending : TupleOrder.Ascending);
                    break;

                case "aopLE":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Descending : TupleOrder.Ascending);
                    break;

                case "aopGT":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Ascending : TupleOrder.Descending);
                    break;

                case "aopGE":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Ascending : TupleOrder.Descending);
                    break;

                default:
                    System.out.println("The FilterOut contains the " + op + " operator");
                    break;
            }
        }

        return sortingDir;
    }

    private short SetOffset(CondExpr outFilter[]) {
        short offset = 0;

        for (int i = 0; i < (outFilter.length - 1); i++) {
            String op = outFilter[i].op.toString();

            switch (op) {

                case "aopLT":
                    offset = 1;
                    break;

                case "aopGT":
                    offset = 1;
                    break;

                default:
                    break;
            }
        }

        return offset;
    }

    /**
     * @exception PageNotReadException    exception from lower layer
     * @exception TupleUtilsException     exception from using tuple utilities
     * @exception PredEvalException       exception from PredEval class
     * @exception SortException           sort exception
     * @exception LowMemException         memory error
     * @exception UnknowAttrType          attribute type unknown
     * @exception UnknownKeyTypeException key type unknown
     * @exception Exception               other exceptions
     * 
     */
    public Tuple get_next() throws IOException, JoinsException, IndexException, InvalidTupleSizeException,
            InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, SortException,
            LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception {

        if (first){
            first = false;
            visited = false;
            L2pos = 0;
        }
        
        // Assuming to keep track of position on L1 and the scanned position so far
        while (L2pos < rel_len) {
            if (visited == false) {
                visited = true;
                L1pos = perm_arr[L2pos];
                bitmap.setBit(L1pos);
                bit_out = bitmap.getValidPositions(L1pos + Off);
            }

            if (bit_out.hasNext()){
                int i = bit_out.next();
                Projection.Join(list_1.get(i), _in1, list_1.get(perm_arr[L2pos]), _in2, Jtuple, perm_mat, nOutFlds);
                return Jtuple;
            }

            L2pos++;
            visited = false;
        }

        return null;
    }

    /**
     * implement the abstract method close() from super class Iterator to finish
     * cleaning up
     * 
     * @exception IOException    I/O error from lower layers
     * @exception JoinsException join error from lower layers
     * @exception IndexException index access error
     */
    public void close() throws JoinsException, IOException, IndexException {
        if (!closeFlag) {

            try {
                outer.close();
            } catch (Exception e) {
                throw new JoinsException(e, "IESelfJoin2.java: error in closing iterator.");
            }
            closeFlag = true;
        }
    }
}
