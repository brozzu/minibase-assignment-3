package iterator;

import heap.*;
import global.*;
import bufmgr.*;
import diskmgr.*;
import index.*;
import java.lang.*;
import java.net.SocketImpl;
import java.io.*;

import java.util.*;

/**
 *
 * This file contains an implementation of the nested loops join algorithm as
 * described in the Shapiro paper. The algorithm is extremely simple:
 *
 * foreach tuple r in R do foreach tuple s in S do if (ri == sj) then add (r, s)
 * to the result.
 */

public class IEJoin extends Iterator {
    private AttrType _in1[], _in2[];
    private Tuple Jtuple; // Joined tuple
    private FldSpec perm_mat[];
    private int nOutFlds;

    // Aggiunte da Brozzu
    private ArrayList<Tuple> L1, L1p, L2, L2p;
    private int[] P, Pp, O, O2;
    private short[] Bp;
    private BitMap bitmap;
    private java.util.Iterator<Integer> bit_out;
    private int n, m, i;
    private short eqOff;
    private boolean getNextUsed, i_visited;

    /**
     * constructor Initialize the two relations which are joined, including relation
     * type,
     * 
     * @param in1          Array containing field types of R.
     * @param len_in1      # of columns in R.
     * @param t1_str_sizes shows the length of the string fields.
     * @param in2          Array containing field types of S
     * @param len_in2      # of columns in S
     * @param t2_str_sizes shows the length of the string fields.
     * @param amt_of_mem   IN PAGES
     * @param am1          access method for left i/p to join
     * @param relationName access hfapfile for right i/p to join
     * @param outFilter    select expressions
     * @param rightFilter  reference to filter applied on right i/p
     * @param proj_list    shows what input fields go where in the output tuple
     * @param n_out_flds   number of outer relation fileds
     * @throws Exception
     * @throws UnknownKeyTypeException
     * @throws UnknowAttrType
     * @throws LowMemException
     * @throws PredEvalException
     * @throws TupleUtilsException
     * @throws PageNotReadException
     * @throws InvalidTypeException
     * @throws InvalidTupleSizeException
     * @throws IndexException
     * @throws JoinsException
     * @exception IEJoinException exception from this class (To Implement)
     */
    public IEJoin(AttrType in1[], int len_in1, short t1_str_sizes[], AttrType in2[], int len_in2, short t2_str_sizes[],
            int amt_of_mem, String heapfile1, String heapfile2, CondExpr outFilter[], CondExpr rightFilter[],
            FldSpec proj_list[], int n_out_flds) throws JoinsException, IndexException, InvalidTupleSizeException,
            InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, LowMemException,
            UnknowAttrType, UnknownKeyTypeException, Exception {

        // Flag
        getNextUsed = false;

        // Set Offset Based on Predicates
        eqOff = SetOffset(outFilter);

        // Create the 4 iterators to be sorted
        FileScan[] file1Itr = createStreams(heapfile1, in1, len_in1, t1_str_sizes, 2);
        FileScan[] file2Itr = createStreams(heapfile2, in2, len_in2, t2_str_sizes, 2);

        // Set Ascending based on Op
        TupleOrder[] sortingDir = SetSortingDirection(outFilter);
        TupleOrder[] secondaryKeyDir = SetSecondarySortingDirection(outFilter);

        Tuple tuple = new Tuple();
        Iterator tmp = null;

        // L1 array created
        // Check relation of amt_of_mem wrt to other things.... (WIP)
        tmp = new Sort(in1, (short) len_in1, t1_str_sizes, file1Itr[0], outFilter[1].operand1.symbol.offset,
                secondaryKeyDir[0], t1_str_sizes[0], amt_of_mem);
        Iterator L1Itr = new Sort(in1, (short) len_in1, t1_str_sizes, tmp, outFilter[0].operand1.symbol.offset,
                sortingDir[0], t1_str_sizes[0], amt_of_mem);
        L1 = new ArrayList<Tuple>();
        tuple = null;
        while ((tuple = L1Itr.get_next()) != null) {
            Tuple x = new Tuple(tuple);
            L1.add(x);
        }

        //L1Itr.close();
        //file1Itr[0].close();

        tmp = new Sort(in1, (short) len_in1, t1_str_sizes, file1Itr[1], outFilter[0].operand1.symbol.offset,
                secondaryKeyDir[1], t1_str_sizes[0], amt_of_mem);
        Iterator L2Itr = new Sort(in1, (short) len_in1, t1_str_sizes, tmp, outFilter[1].operand1.symbol.offset,
                sortingDir[1], t1_str_sizes[0], amt_of_mem);

        L2 = new ArrayList<Tuple>();
        tuple = null;
        while ((tuple = L2Itr.get_next()) != null) {
            Tuple x = new Tuple(tuple);
            L2.add(x);
        }

        //L2Itr.close();
        //file1Itr[1].close();

        tmp = new Sort(in2, (short) len_in2, t2_str_sizes, file2Itr[0], outFilter[1].operand2.symbol.offset,
                secondaryKeyDir[0], t2_str_sizes[0], amt_of_mem);
        Iterator L1pItr = new Sort(in2, (short) len_in2, t2_str_sizes, tmp, outFilter[0].operand2.symbol.offset,
                sortingDir[0], t2_str_sizes[0], amt_of_mem);

        L1p = new ArrayList<Tuple>();
        tuple = null;
        while ((tuple = L1pItr.get_next()) != null) {
            Tuple x = new Tuple(tuple);
            L1p.add(x);
        }

        //L1pItr.close();
        //file2Itr[0].close();


        tmp = new Sort(in2, (short) len_in2, t2_str_sizes, file2Itr[1], outFilter[0].operand2.symbol.offset,
                secondaryKeyDir[1], t2_str_sizes[0], amt_of_mem);
        Iterator L2pItr = new Sort(in2, (short) len_in2, t2_str_sizes, tmp, outFilter[1].operand2.symbol.offset,
                sortingDir[1], t2_str_sizes[0], amt_of_mem);
        L2p = new ArrayList<Tuple>();
        tuple = null;
        while ((tuple = L2pItr.get_next()) != null) {
            Tuple x = new Tuple(tuple);
            L2p.add(x);
        }

        //L2pItr.close();
        //file2Itr[1].close();

        m = L1.size();
        n = L1p.size();

        P = createPermutationArray(L2, L1, m, in1);
        Pp = createPermutationArray(L2p, L1p, n, in2);

        O = createOffsetArray(in1, L1, in2, L1p, outFilter[0].operand1.symbol.offset,
                outFilter[0].operand2.symbol.offset, sortingDir[0]);
        O2 = createOffsetArray(in1, L2, in2, L2p, outFilter[1].operand1.symbol.offset,
                outFilter[1].operand2.symbol.offset, sortingDir[1]);

        Bp = new short[n];
        bitmap = new BitMap(n,10);

        // Copy field types
        _in1 = new AttrType[in1.length];
        _in2 = new AttrType[in2.length];
        System.arraycopy(in1, 0, _in1, 0, in1.length);
        System.arraycopy(in2, 0, _in2, 0, in2.length);
      
        Jtuple = new Tuple();
    
        AttrType[] Jtypes = new AttrType[n_out_flds];
        short[] t_size;

        perm_mat = proj_list;
        nOutFlds = n_out_flds;

        try {
            t_size = TupleUtils.setup_op_tuple(Jtuple, Jtypes, in1, len_in1, in2, len_in2, t1_str_sizes, t2_str_sizes,
                    proj_list, nOutFlds);
        } catch (TupleUtilsException e) {
            // throw new IEJoinException(e, "TupleUtilsException is caught by
            // IEJoin.java");
        }

        /*System.out.println("Permutation Array P (L2 -> L1):" + Arrays.toString(P));
        System.out.println("Permutation Array Pp (L2p -> L1p):" + Arrays.toString(Pp));

        System.out.println("Offset Array L1 -> L1p:" + Arrays.toString(O));
        System.out.println("Offset Array L2 -> L2p:" + Arrays.toString(O2));

        System.out.println("L1:");
        print_tuple_arrayList(L1, in1);
        System.out.println("\n");

        System.out.println("L2:");
        print_tuple_arrayList(L2, in1);
        System.out.println("\n");

        System.out.println("L1p:");
        print_tuple_arrayList(L1p, in2);
        System.out.println("\n");

        System.out.println("L2p:");
        print_tuple_arrayList(L2p, in2);
        System.out.println("\n");*/
    }

    /**
     * @exception PageNotReadException    exception from lower layer
     * @exception TupleUtilsException     exception from using tuple utilities
     * @exception PredEvalException       exception from PredEval class
     * @exception SortException           sort exception
     * @exception LowMemException         memory error
     * @exception UnknowAttrType          attribute type unknown
     * @exception UnknownKeyTypeException key type unknown
     * @exception Exception               other exceptions
     * 
     */
    public Tuple get_next() throws IOException, JoinsException, IndexException, InvalidTupleSizeException,
            InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, SortException,
            LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception {

        if (!getNextUsed) {
            i = 0;
            i_visited = false;
            getNextUsed = true;
        }

        while (i < m) {

            if (!i_visited) {
                // Set Bit relative to visited Tuples in L1p
                int off2 = O2[i]; // L2 -> L2p
                for (int k = 0; k < Math.min(off2, n); k++) {
                    Bp[Pp[k]] = 1;
                    bitmap.setBit(Pp[k]);
                }

                int off1 = O[P[i]]; // L1 -> L1p
                bit_out = bitmap.getValidPositions(off1 + eqOff);

                i_visited = true;
            }

            
            if(bit_out.hasNext()){
                int j = bit_out.next();
                Projection.Join(L2.get(i), _in1, L1p.get(j), _in2, Jtuple, perm_mat, nOutFlds);
                return Jtuple;

            }


            i++;
            i_visited = false;
        }

        return null;
    }

    /**
     * implement the abstract method close() from super class Iterator to finish
     * cleaning up
     * 
     * @exception IOException    I/O error from lower layers
     * @exception JoinsException join error from lower layers
     * @exception IndexException index access error
     */
    public void close() throws JoinsException, IOException, IndexException {
        if (!closeFlag) {

            try {
                //outer.close();
            } catch (Exception e) {
                throw new JoinsException(e, "IEJoin.java: error in closing iterator.");
            }
            closeFlag = true;
        }
    }

    private TupleOrder[] SetSortingDirection(CondExpr outFilter[]) {
        TupleOrder[] sortingDir = new TupleOrder[outFilter.length - 1];

        for (int i = 0; i < (outFilter.length - 1); i++) {
            String op = outFilter[i].op.toString();

            switch (op) {

                case "aopLT":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Descending : TupleOrder.Ascending);
                    break;

                case "aopLE":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Descending : TupleOrder.Ascending);
                    break;

                case "aopGT":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Ascending : TupleOrder.Descending);
                    break;

                case "aopGE":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Ascending : TupleOrder.Descending);
                    break;

                default:
                    System.out.println("The FilterOut contains the " + op + " operator");
                    break;
            }
        }
        return sortingDir;
    }

    private TupleOrder[] SetSecondarySortingDirection(CondExpr outFilter[]) {
        TupleOrder[] sortingDir = new TupleOrder[2];


        switch(outFilter[0].op.toString()){
            case "aopLT":
                switch (outFilter[1].op.toString()){
                    case "aopLT":
                        sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                        sortingDir[1] = new TupleOrder(TupleOrder.Descending);
                        break;

                    case "aopLE":
                        sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                        sortingDir[1] = new TupleOrder(TupleOrder.Ascending);
                        break;

                    case "aopGT":
                        sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                        sortingDir[1] = new TupleOrder(TupleOrder.Descending);
                        break;

                    case "aopGE":
                        sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                        sortingDir[1] = new TupleOrder(TupleOrder.Ascending);
                        break;

                    default:
                        System.out.println("The FilterOut contains the not expected operator");
                        break;
                }
                break;

                case "aopLE":
                    switch (outFilter[1].op.toString()){
                        case "aopLT":
                            sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Descending);
                            break;

                        case "aopLE":
                            sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Ascending);
                            break;

                        case "aopGT":
                            sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Descending);
                            break;

                        case "aopGE":
                            sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Ascending);                            
                        break;

                        default:
                            System.out.println("The FilterOut contains the not expected operator");        
                            break;
                    }
                    break;

                case "aopGT":
                    switch (outFilter[1].op.toString()){
                        case "aopLT":
                            sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Ascending);                            
                            break;

                        case "aopLE":
                            sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Descending);                            
                            break;

                        case "aopGT":
                            sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Ascending);                            
                            break;

                        case "aopGE":
                            sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Descending);                            
                        break;

                        default:
                            System.out.println("The FilterOut contains the not expected operator");
                            break;
                    }
                    break;

                case "aopGE":
                    switch (outFilter[1].op.toString()){
                        case "aopLT":
                            sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Ascending);                            
                            break;

                        case "aopLE":
                            sortingDir[0] = new TupleOrder(TupleOrder.Descending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Descending);                            
                            break;

                        case "aopGT":
                            sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Ascending);                            
                            break;

                        case "aopGE":
                            sortingDir[0] = new TupleOrder(TupleOrder.Ascending);
                            sortingDir[1] = new TupleOrder(TupleOrder.Descending);                            
                            break;

                        default:
                            System.out.println("The FilterOut contains the not expected operator");
                            break;
                    }
                    break;

                default:
                    System.out.println("The FilterOut contains the not expected operator");
                    break;
            
        }


        for (int i = 0; i < (outFilter.length - 1); i++) {
            String op = outFilter[i].op.toString();

            switch (op) {

                case "aopLT":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Descending : TupleOrder.Ascending);
                    break;

                case "aopLE":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Descending : TupleOrder.Ascending);
                    break;

                case "aopGT":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Ascending : TupleOrder.Descending);
                    break;

                case "aopGE":
                    sortingDir[i] = new TupleOrder(i % 2 == 0 ? TupleOrder.Ascending : TupleOrder.Descending);
                    break;

                default:
                    System.out.println("The FilterOut contains the " + op + " operator");
                    break;
            }
        }
        return sortingDir;
    }

    private FileScan[] createStreams(String heapfileName, AttrType in[], int len_in, short str_sizes[],
            int n_iterators) {
        FileScan[] iterators = new FileScan[n_iterators];

        FldSpec[] Sprojection = new FldSpec[len_in];
        for (int i = 0; i < len_in; i++) {
            Sprojection[i] = new FldSpec(new RelSpec(RelSpec.outer), i + 1);
        }

        for (int i = 0; i < n_iterators; i++) {
            try {
                iterators[i] = new FileScan(heapfileName, in, str_sizes, (short) len_in, (short) len_in, Sprojection,
                        null);
            } catch (Exception e) {
                System.err.println("" + e);
                Runtime.getRuntime().exit(1);
            }
        }

        return iterators;
    }

    private int[] createPermutationArray(ArrayList<Tuple> list_1, ArrayList<Tuple> list_2, int rel_len, AttrType[] in)
            throws UnknowAttrType, TupleUtilsException, IOException {
        int[] perm_arr = new int[rel_len];
        for (int i = 0; i < rel_len; i++) {
            for (int j = 0; j < rel_len; j++) {
                boolean equal = true;

                // Compare each field
                for (int k = 0; k < in.length; k++) {

                    int cmp = TupleUtils.CompareTupleWithTuple(in[k], list_1.get(i), k + 1, list_2.get(j), k + 1);

                    if (cmp != 0) {
                        equal = false;
                    }
                }

                if (equal) {
                    perm_arr[i] = j;
                }

            }
        }

        return perm_arr;
    }

    private int[] createOffsetArray(AttrType[] in1, ArrayList<Tuple> list_1, AttrType[] in2, ArrayList<Tuple> list_2,
            int offset_1, int offset_2, TupleOrder Sorting_order)
            throws UnknowAttrType, TupleUtilsException, IOException {
        int[] Offset = new int[list_1.size()];

        for (int i = 0; i < list_1.size(); i++) {
            boolean found = false;

            for (int j = 0; j < list_2.size() && !found; j++) {
                // Compare each field

                if (((Sorting_order.tupleOrder == TupleOrder.Descending)
                        && TupleUtils.CompareTupleWithTuple(in1[offset_1 - 1], list_1.get(i), offset_1, list_2.get(j),
                                offset_2) >= 0)
                        || ((Sorting_order.tupleOrder == TupleOrder.Ascending)
                                && TupleUtils.CompareTupleWithTuple(in1[offset_1 - 1], list_1.get(i), offset_1,
                                        list_2.get(j), offset_2) <= 0)) {
                    found = true;
                    Offset[i] = j;
                    break;
                }
            }

            if (!found) {
                Offset[i] = list_1.size();
            }

        }

        return Offset;
    }

    private short SetOffset(CondExpr outFilter[]) {
        short offset = 0;

        for (int i = 0; i < (outFilter.length - 1); i++) {
            String op = outFilter[i].op.toString();

            switch (op) {

                case "aopLT":
                    offset = 1;
                    break;

                case "aopGT":
                    offset = 1;
                    break;

                default:
                    break;
            }
        }

        return offset;
    }

    private void print_tuple_arrayList(ArrayList<Tuple> L, AttrType[] in) {
        L.stream().forEach(item -> {
            // Compare each field

            try {
                item.print(in);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        });
    }

}
