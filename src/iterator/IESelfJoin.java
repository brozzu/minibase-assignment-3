package iterator;

import heap.*;
import global.*;
import bufmgr.*;
import diskmgr.*;
import index.*;
import java.lang.*;
import java.net.SocketImpl;
import java.io.*;

import java.util.*;

/**
 *
 * This file contains an implementation of the nested loops join algorithm as
 * described in the Shapiro paper. The algorithm is extremely simple:
 *
 * foreach tuple r in R do foreach tuple s in S do if (ri == sj) then add (r, s)
 * to the result.
 */

public class IESelfJoin extends Iterator {
  private AttrType _in1[], _in2[];
  private int in1_len, in2_len;
  private Iterator outer;
  private short t2_str_sizescopy[];
  private CondExpr OutputFilter[];
  private CondExpr RightFilter[];
  private int n_buf_pgs; // # of buffer pages available.
  private boolean done, // Is the join complete
      get_from_outer; // if TRUE, a tuple is got from outer
  private Tuple outer_tuple, inner_tuple;
  private Tuple Jtuple; // Joined tuple
  private FldSpec perm_mat[];
  private int nOutFlds;
  private Heapfile hf;
  private Scan inner;
  // Aggiunte da Brozzu
  private ArrayList<Tuple> list_1, list_2, tuplesJoinList;
  private TupleOrder ascending;
  private int[] perm_arr;
  private short[] bit_arr;
  private int rel_len;

  private int L1pos;
  private int L2pos;
  private short Off;

  /**
   * constructor Initialize the two relations which are joined, including relation
   * type,
   * 
   * @param in1          Array containing field types of R.
   * @param len_in1      # of columns in R.
   * @param t1_str_sizes shows the length of the string fields.
   * @param in2          Array containing field types of S
   * @param len_in2      # of columns in S
   * @param t2_str_sizes shows the length of the string fields.
   * @param amt_of_mem   IN PAGES
   * @param am1          access method for left i/p to join
   * @param relationName access hfapfile for right i/p to join
   * @param outFilter    select expressions
   * @param rightFilter  reference to filter applied on right i/p
   * @param proj_list    shows what input fields go where in the output tuple
   * @param n_out_flds   number of outer relation fileds
   * @throws Exception
   * @throws UnknownKeyTypeException
   * @throws UnknowAttrType
   * @throws LowMemException
   * @throws PredEvalException
   * @throws TupleUtilsException
   * @throws PageNotReadException
   * @throws InvalidTypeException
   * @throws InvalidTupleSizeException
   * @throws IndexException
   * @throws JoinsException
   * @exception IESelfJoinException exception from this class (To Implement)
   */
  public IESelfJoin(AttrType in1[], int len_in1, short t1_str_sizes[], AttrType in2[], int len_in2, short t2_str_sizes[],
      int amt_of_mem, Iterator am1, Iterator am2, CondExpr outFilter[], CondExpr rightFilter[], FldSpec proj_list[],
      int n_out_flds)
      throws JoinsException, IndexException, InvalidTupleSizeException, InvalidTypeException, PageNotReadException,
      TupleUtilsException, PredEvalException, LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception {

    //Index set for getNext()
    L1pos = 0;
    L2pos = -1;

    // Set offset if strict inequality is present
    Off = SetOffset(outFilter);

    // Set Ascending based on Op
    TupleOrder[] sortingDir = SetSortingDirection(outFilter);


    list_1 = new ArrayList<Tuple>();
    // L1 array created
    Tuple tuple = new Tuple();
    tuple = null;
    // Check relation of amt_of_mem wrt to other things.... (WIP)
    Iterator L1 = new Sort(in1, (short) len_in1, t1_str_sizes, am1, outFilter[0].operand1.symbol.offset, sortingDir[0], 0, amt_of_mem);

    while ((tuple = L1.get_next()) != null) {
      Tuple x = new Tuple(tuple);
      list_1.add(x);
    }

    L1.close();

    System.out.println("List 1 Len" + list_1.size());

    rel_len = list_1.size();

    // Copy field types
    _in1 = new AttrType[in1.length];
    _in2 = new AttrType[in2.length];
    System.arraycopy(in1, 0, _in1, 0, in1.length);
    System.arraycopy(in2, 0, _in2, 0, in2.length);
    in1_len = len_in1;
    in2_len = len_in2;

    // Access oterator
    outer = am1;
    t2_str_sizescopy = t2_str_sizes;
    inner_tuple = new Tuple();
    Jtuple = new Tuple();
    OutputFilter = outFilter;
    RightFilter = rightFilter;

    n_buf_pgs = amt_of_mem;
    inner = null;
    done = false;
    get_from_outer = true;

    AttrType[] Jtypes = new AttrType[n_out_flds];
    short[] t_size;

    perm_mat = proj_list;
    nOutFlds = n_out_flds;

    try {
      t_size = TupleUtils.setup_op_tuple(Jtuple, Jtypes, in1, len_in1, in2, len_in2, t1_str_sizes, t2_str_sizes,
          proj_list, nOutFlds);
    } catch (TupleUtilsException e) {
      // throw new IESelfJoinException(e, "TupleUtilsException is caught by IESelfJoin.java");
    }

  }

  private TupleOrder[] SetSortingDirection( CondExpr outFilter[]){
    TupleOrder[] sortingDir = new TupleOrder[outFilter.length-1];

    for( int i = 0; i < (outFilter.length-1); i++){
      String op = outFilter[i].op.toString();

      switch(op){

        case "aopLT":
          sortingDir[i] = new TupleOrder( i%2==0 ? TupleOrder.Descending : TupleOrder.Ascending);
          break;

        case "aopLE":
          sortingDir[i] = new TupleOrder( i%2==0 ? TupleOrder.Descending : TupleOrder.Ascending);
          break;

        case "aopGT":
          sortingDir[i] = new TupleOrder( i%2==0 ? TupleOrder.Ascending : TupleOrder.Descending);
          break;

        case "aopGE":
          sortingDir[i] = new TupleOrder( i%2==0 ? TupleOrder.Ascending : TupleOrder.Descending);
          break;

        default:
          System.out.println("The FilterOut contains the " + op + " operator");
          break;
      }
    }

    return sortingDir;
  }


  private short SetOffset( CondExpr outFilter[]){
    short offset = 0;

    for( int i = 0; i < (outFilter.length-1); i++){
      String op = outFilter[i].op.toString();

      switch(op){

        case "aopLT":
          offset = 1;
          break;

        case "aopGT":
          offset = 1;
          break;

        default:
          break;
      }
    }

    return offset;
  }



  /**
   * @exception PageNotReadException    exception from lower layer
   * @exception TupleUtilsException     exception from using tuple utilities
   * @exception PredEvalException       exception from PredEval class
   * @exception SortException           sort exception
   * @exception LowMemException         memory error
   * @exception UnknowAttrType          attribute type unknown
   * @exception UnknownKeyTypeException key type unknown
   * @exception Exception               other exceptions
   * 
   */
  public Tuple get_next() throws IOException, JoinsException, IndexException, InvalidTupleSizeException,
      InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, SortException,
      LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception {

    // Assuming to keep track of position on L1 and the scanned position so far
    while (L1pos < rel_len) {
        if (L2pos < 0) {
            L2pos = L1pos;
        }

        while (L2pos >= 0 && L2pos + Off < rel_len)
        {
            Projection.Join(list_1.get(L2pos + Off), _in1, 
                list_1.get(L1pos), _in1, 
                Jtuple, perm_mat, nOutFlds);

            L2pos++;
            return Jtuple;
        } 

        L1pos++;
        L2pos = -1;
    }

    return null;
  }

  /**
   * implement the abstract method close() from super class Iterator to finish
   * cleaning up
   * 
   * @exception IOException    I/O error from lower layers
   * @exception JoinsException join error from lower layers
   * @exception IndexException index access error
   */
  public void close() throws JoinsException, IOException, IndexException {
    if (!closeFlag) {

      try {
        outer.close();
      } catch (Exception e) {
        throw new JoinsException(e, "IESelfJoin.java: error in closing iterator.");
      }
      closeFlag = true;
    }
  }
}

